FROM debian:bookworm-slim

COPY rsyslog.gpg /usr/share/keyrings/rsyslog.gpg
COPY deb_autistici_org.gpg /usr/share/keyrings/deb.autistici.org.gpg
COPY sources.list /etc/apt/sources.list.d/rsyslog.list

RUN env http_proxy= apt-get -q update && \
    env http_proxy= DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends \
        rsyslog rsyslog-gnutls rsyslog-relp \
        rsyslog-elasticsearch rsyslog-omclickhouse rsyslog-omhttp \
        rsyslog-mmjsonparse rsyslog-mmutf8fix \
        rsyslog-mmnormalize rsyslog-mmrm1stspace rsyslog-mmfields \
        rsyslog-exporter && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/*

ENV TZ=UTC

ENTRYPOINT ["/usr/sbin/rsyslogd", "-n", "-iNONE"]

