#!/bin/sh

# script is called from top-level dir.
logdir=./test/logs
user=$(whoami)
logfile=${logdir}/${user}.log

set -e
set -x

logger --server 127.0.0.1 --port 6514 --rfc5424 --tcp "hello world"

test -e ${logfile}

grep -q "hello world" ${logfile}

# Show the log file for debugging purposes.
cat ${logfile}

exit 0

